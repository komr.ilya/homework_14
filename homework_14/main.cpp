//
//  main.cpp
//  homework_14
//
//  Created by Ilya Komrakov on 26.04.2020.
//  Copyright © 2020 Ilya Komrakov. All rights reserved.
//

#include <iostream>
#include <string>


int main()
{

    std::string name = "abcdefghi";
    
    std::cout << name << std::endl;
    std::cout << name.length() << std::endl;
    std::cout << name.substr(0, 1) << std::endl;
    std::cout << name.substr(name.length() - 1, 1) << std::endl;
    
    return 0;
    
}
